%include "lib.inc"
%include "words.inc"

section .rodata
key_err: db "No such key!", 0
size_err: db "Invalid length!", 0

section .bss
buffer: resb buffer_size

section .text
global _start
extern find_word

_start:
	mov rdi, buffer
	mov rsi, buffer_size
	call read_word
	sub rax, 0
	jz .size_error
	mov rdi, rax
	mov rsi, VAL
	push rdx	
	call find_word
	pop rdx
	sub rax, 0
	jz .key_error	
	add rax, 9
	add rax, rdx
	mov rdi, rax
	call print_string
	call print_newline
	xor rdi, rdi
	jmp .end		
	.key_error:
		mov rdi, key_err
		jmp .err
	.size_error:
		mov rdi, size_err
	.err:
		call print_err
		call print_newline
		mov rdi, 1
	.end:
		call exit
