section .text
global find_word
extern string_equals

find_word:
	.loop:
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi	
		sub rax, 0
		jnz .answer	
		mov rsi, [rsi]
		sub rsi, 0
		jnz .loop	
		xor rax, rax
		jmp .return
	.answer:
		mov rax, rsi
	.return:
		ret