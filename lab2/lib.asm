global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_err

section .text
  
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
	.loop:
		cmp byte [rdi + rax], 0
		je .return
		inc rax
		jmp .loop
	.return:
		ret
		
print_err:
    call string_length
	mov rdx, rax
    mov rsi, rdi
	mov rax, 1
    mov rdi, 2
    syscall
    ret		
		
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
	mov rdx, rax
    mov rsi, rdi
	mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rdx, 1
    mov rdi, 1
    mov rax, 1
	mov rsi , rsp
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rcx, rcx 
    mov rax, rdi
    mov r9, 10
    .loop:
        xor rdx, rdx
		inc rcx
		div r9
		push rdx
		cmp rax, 0
		je .print
		jmp .loop
    .print:
        pop rdx
		add rdx, '0'
        mov rdi, rdx
		push rcx
		call print_char
		pop rcx
		dec rcx
        jnz .print
		ret
	
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0
    jge .not_negative
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .not_negative:
        call print_uint
        ret	

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
	xor rcx, rcx
    xor rdx, rdx
	.loop:
		mov r9b, [rdi + rcx]
		mov r10b, [rsi + rcx]
		cmp r10b, r9b
		jne .zero
		cmp r10b, 0
		je .return
		inc rcx
		jmp .loop
	.zero:
		xor rax, rax
		ret
	.return:
		inc rax
		ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push rax
    mov rsi, rsp
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	xor rdx,rdx
    xor rax, rax
    .loop:
        push rsi
        push rdi
		push rdx
        call read_char
		pop rdx
        pop rdi
        pop rsi
        cmp rax, 0x20
        je .space
        cmp rax, 0x9
        je .space
        cmp rax, 0xA
        je .space
        cmp rax, 0
        je .return
        mov byte[rdi+rdx], al
        cmp rdx, rsi
        je .zero
        inc rdx
        jmp .loop
    .space:
        cmp rdx, 0
	    je .loop
        jmp .return
    .return:
        mov byte[rdi+rdx], 0
        mov rax, rdi
        ret		
    .zero:
        xor rax,rax
        ret 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    xor rax, rax
    xor rdx, rdx
    mov r9, 10
    .loop:
		mov dl, byte[rcx+rdi]
		cmp rdx, '0'
		jl .return
		cmp rdx, '9'
		jg .return
		sub rdx, '0'
		push rdx
		mul r9 
		pop rdx
		add rax, rdx
		inc rcx
		jmp .loop
    .return:
		mov rdx, rcx
		ret 

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; указатель строки - rdi
; указатель буфера - rsi
; длина буфера - rdx
string_copy:
	push rbx
	push rdi
	push rsi
	call string_length
	pop rsi
	pop rdi
	cmp rax, rdx
	jg .zero
	.loop:
		mov rbx, [rdi]
		mov [rsi], rbx
		inc rsi
		inc rdi
		cmp bl, 0
		je .return
		jmp .loop  
	.zero:
		xor rax, rax
	.return:
		pop rbx
		ret